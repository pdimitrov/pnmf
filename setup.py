#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import io
import re
from glob import glob
from os.path import basename, dirname, join, splitext

from setuptools import find_packages, setup


def read(*names, **kwargs):
    return io.open(
        join(dirname(__file__), *names),
        encoding=kwargs.get('encoding', 'utf8')).read()


def get_version():
    '''for now use fixed number'''
    # try:
    #     with open(os.path.abspath('src/tokeme/version.generated')) as f:
    #         version_number = f.read().strip()
    # except FileNotFoundError as e:
    #     raise RuntimeError('The version number was not generated') from e
    # assert version_number, 'The version number was not generated'
    return '0.7.0'  # version_number


setup(
    name='pNMF',
    version=get_version(),
    description='pNMF - probabilistic Non-negative Matrix Factorization (pNMF)',
    long_description=
    'Python package for probabilistic Non-negative Matrix Factorization (pNMF) of (row-)stochastic matrices.',
    author='Peter Dimitrov',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    py_modules=[
        splitext(basename(path))[0]
        for path in glob('src/**/*.py', recursive=True)
        if re.search("__init__", path) is None
    ],
    package_data={'pNMF': ['data/*_data.csv', 'data/*.pic']},
    include_package_data=False,
    zip_safe=True,
    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: Apache',
        'Operating System :: Unix',
        'Operating System :: POSIX',
        # 'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Topic :: Utilities'
    ],
    keywords=['clustering', "NMF", "model selection"],
    install_requires=[
        'pandas>=0.23.0', 'numpy>=1.15.4', 'scipy>=1.1.0', 'autograd>=1.2.0',
        'pymanopt>=0.2.3', 'pytest>=4.0.0', 'tensorflow>=1.13.1'
    ],
    extras_require={
        #   'rst': ['docutils>=0.11'],
    },
    entry_points={
        # 'console_scripts': [
        #     'nameless = nameless.cli:main',
        # ]
    },
    scripts={
        'scripts/process_raw_data_pNMF.py'  # script for automating a common task
    })
