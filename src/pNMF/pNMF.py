from abc import ABC

import numpy as np
import pandas as pd
import scipy.sparse as sp
from scipy.special import logsumexp

from .pmf import Pmf


class pNMF(ABC):
    """Main class for clustering of a range and model selection."""

    def __init__(self,
                 X: sp.csr_matrix,
                 Y: sp.csr_matrix,
                 match_index: np.array,
                 row_weights: np.array = None,
                 row_normalized: bool = True,
                 **kwargs):
        """Configure."""
        self.X = X.copy()
        self.Y = Y.copy()
        self.match_index = match_index
        self.row_normalized = row_normalized
        self.row_weights = row_weights
        self.best_fit = None

    def cluster(self, k, row_normalized=True, **kwargs):
        """cluster for given number of clusters k."""
        print("fitting for k = {}...".format(k))
        pmf = Pmf(
            self.X,
            rank=k,
            # ignore input argument in case it's passed
            row_normalized=self.row_normalized,
            row_weights=self.row_weights,
            **kwargs)
        pmf_fit = pmf()
        logW = np.log(pmf_fit.coef())
        logHt = np.log(pmf_fit.basis().T)
        # return the multinomial likelihood, and pNMF fit
        return logsumexp(logW[self.match_index, :] + self.Y.dot(logHt),
                         1).sum(), pmf_fit

    def range_clustering(self,
                         range_k=range(10, 61),
                         return_all=False,
                         **kwargs):
        """Extract best clustering among the provided range of cluster sizes,
        or, alternatively, returns all fits in the range + their BICs."""
        print("\nClustering with {}-{} components\ninitializing ...".format(
            min(range_k), max(range_k)))
        pW, _ = self.X.shape
        N, pH = self.Y.shape

        def pen_ll(args):
            ll, fit = args
            k = fit.dim()[2]
            return np.log(N) * (pW * (k - 1.) + (pH - 1.) * k) - 2. * ll

        fits = [self.cluster(k, **kwargs) for k in range_k]
        BICs = pd.Series(map(pen_ll, fits), index=range_k)

        def argmax_series(pmf_fit):
            return pd.Series(
                np.asarray(pmf_fit.coef().argmax(1)).squeeze(),
                name="grouping")

        def max_series(pmf_fit):
            return pd.Series(
                np.asarray(pmf_fit.coef().max(1)).squeeze(), name="score")

        if return_all:
            return (
                BICs,  #
                [argmax_series(pmf_fit) for _, pmf_fit in fits],
                fits,
                [max_series(pmf_fit) for _, pmf_fit in fits])
        fit_best = fits[BICs.values.argmin()][0]
        return (BICs, argmax_series(fit_best), fit_best, max_series(fit_best))
