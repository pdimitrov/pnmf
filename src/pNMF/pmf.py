"""
**Weighted Probabilistic Nonnegative Matrix Factorization (PMF).**

This versiong of PMF interprets the approximated matrix (V) as row-stochastic
 (or to be converted to one) with optional row (observation) weights;
 uses weighted KL-divergence for measuring the discrepancies between the
 approximand and the approximators. Factorization is guided by an
 expectation maximization algorithm.
"""
import sys
from types import SimpleNamespace

import numpy as np
from numpy.random import choice
from scipy.special import logsumexp


def _custom_matmul(X, Y, nnz_indices):
    """Fast matrix multiplication of two dense matrices given a set of indexes
    where they need to be multiplied."""
    assert X.shape[1] == Y.shape[0]
    assert nnz_indices[0].max() < X.shape[0]
    assert nnz_indices[1].max() < Y.shape[1]
    return np.sum(X[nnz_indices[0], :] * Y[:, nnz_indices[1]].T, 1)


class Pmf():
    """
    Class to represent the probabilistic matrix factorization.
    """

    def initialize_nmf(self):
        """
        Return initialized basis and mixture matrix. Initialized matrices are
        of the same type as the target matrix.
        """
        N, M = self.V.shape
        try:
            p_c = self.options.p_c
        except AttributeError:
            p_c = int(.125 * M)
        try:
            p_r = self.options.p_r
        except AttributeError:
            p_r = int(.125 * N)
        col_arange = np.arange(M)
        row_arange = np.arange(N)
        W = np.zeros((N, self.options.rank), dtype='float')
        H = np.zeros((self.options.rank, M), dtype='float')
        for i in range(self.options.rank):
            W[:, i] = np.asarray(
                self.V[:, choice(col_arange, p_c)].mean(axis=1)).squeeze()
            H[i, :] = np.asarray(
                self.V[choice(row_arange, p_r), :].mean(axis=0)).squeeze()
        return W, H

    def __init__(self,
                 V,
                 W=None,
                 H=None,
                 rank=30,
                 max_iter=2000,
                 min_residuals=1e-5,
                 test_conv=20,
                 n_run=1,
                 verbose=2,
                 row_normalized=None,
                 row_weights=None,
                 eps=None,
                 rel_error=1e-5,
                 run_alt_optim=None,
                 random_seed=15621,
                 model_type="KL",
                 **kwargs):
        """
        :param V: The target matrix to factorize.
        :type V: Instance of :class:`scipy.sparse`, :class:`numpy.ndarray`,
        :class:`numpy.matrix`.

        :param W: Specify initial factorization of basis matrix W.
        :type W: :class:`scipy.sparse`, :class:`numpy.ndarray`,
            :class:`numpy.matrix`, or None (default).

        :param H: Specify initial factorization of mixture matrix H.
        :type H: :class:`scipy.sparse`, :class:`numpy.ndarray`,
        :class:`numpy.matrix`, of None (default).

        :option rank: The factorization rank = W.shape[1] = H.shape[0],
            (default is 30).
        :type rank: `int`

        :option n_run: Number of runs for the factorization algorithm
        (1 by default). The fitted factorization model with the lowest
            objective function value is retained.
        :type n_run: `int`

        Factorization terminates if any of specified criteria is satisfied.

        :option max_iter: Maximum number of factorization iterations.
            Default is 2000.
        :type max_iter: `int`

        :option min_residuals: Minimal required improvement in the norm of the
            residual discrepancy between the target matrix and its MF. Default
            is None.
        :type min_residuals: `float`

        """
        self.name = "pNMF"
        np.random.seed(random_seed)
        self.V = V.copy().astype("float64")
        self.options = SimpleNamespace(
            **{
                **kwargs,
                **{
                    "seed":
                    random_seed,
                    "verbose":
                    int(verbose),
                    "row_normalized":
                    row_normalized,
                    "test_conv":
                    20 if test_conv is None else test_conv,
                    "run_alt_optim":
                    run_alt_optim,
                    "min_residuals":
                    min_residuals,
                    "max_iter":
                    max_iter,
                    "rank":
                    rank,
                    "n_run":
                    n_run,
                    "rel_error":
                    rel_error,
                    "eps":
                    10 * np.finfo(self.V.dtype).eps if eps is None else eps,
                    "model_type":
                    model_type
                }
            })
        if self.options.verbose:
            print(self.options)
        self.W = None
        self.H = None
        row_sums = np.asarray(self.V.sum(1))
        self.row_weights = np.ones((V.shape[0], 1), dtype="float") if row_weights is None\
            else row_weights.reshape((V.shape[0], 1)).astype("float")
        if not self.options.row_normalized:
            self.row_weights *= row_sums
            self.V = self.V.multiply(1. / row_sums).tocsr()
        self.Vrw = self.V \
            if row_weights is None and self.options.row_normalized \
            else self.V.multiply(self.row_weights * row_sums).tocsr()
        self.obj_offset = np.dot(self.Vrw.data, np.log(self.V.data))
        self.final_obj = None
        self.n_iter = None
        self.W_best = None
        self.H_best = None

    def _run_alt_optim(self, maxtime=1800, mingradnorm=1e-7, **kwargs):
        from pymanopt import Problem
        from pymanopt.manifolds import Euclidean, Product
        from pymanopt.solvers import ConjugateGradient
        if self.options.run_alt_optim == "AD":
            # run Autograd optimization
            import autograd.numpy as anp
            from autograd.scipy.misc import logsumexp as ag_logsumexp

            Vrw = np.asarray(self.Vrw.todense())
            if self.options.model_type == "JS":
                V = np.maximum(np.asarray(self.V.todense()), self.options.eps)

            def cost(x):
                W = anp.exp(x[0] - ag_logsumexp(x[0], 1, keepdims=True))
                H = anp.exp(x[1] - ag_logsumexp(x[1], 1, keepdims=True))
                V_est = anp.dot(W, H)
                if self.options.model_type == "KL":
                    crit = self.obj_offset - anp.sum(Vrw * anp.log(V_est))
                else:

                    def KL_div(X, Y):
                        return anp.sum(X * (anp.log(X) - anp.log(Y)), 1)

                    V_m = .5 * (V + V_est)
                    crit = .5 * anp.sum(KL_div(V, V_m) + KL_div(V_est, V_m))
                return crit / Vrw.shape[0]

            arg = None
        else:
            # run TensorFlow optimization
            import tensorflow as tf

            # create the two empty Tensorflow variables W and H to be fed by
            # the Pymanopt's Problem/Solver instances below
            W = tf.Variable(
                tf.placeholder(self.W.dtype, self.W.shape), name="W")
            H = tf.Variable(
                tf.placeholder(self.H.dtype, self.H.shape), name="H")
            V_est = tf.matmul(
                tf.exp(W - tf.reduce_logsumexp(W, 1, keepdims=True)),
                tf.exp(H - tf.reduce_logsumexp(H, 1, keepdims=True)))
            if self.options.model_type == "KL":
                cost = tf.subtract(
                    tf.constant(self.obj_offset),
                    tf.reduce_sum(
                        tf.constant(self.Vrw.todense()) * tf.log(
                            tf.clip_by_value(V_est, self.options.eps, 1.))))
            else:

                def KL_div(X, Y):
                    res = tf.reduce_sum(X * (tf.log(X) - tf.log(Y)), 1)
                    # tf.debugging.assert_all_finite(res, "not all finite")
                    return res

                V = tf.constant(np.maximum(self.V.todense(), self.options.eps))
                V_m = .5 * (V + V_est)
                cost = .5 * tf.reduce_sum(KL_div(V, V_m) + KL_div(V_est, V_m))
            cost = tf.divide(cost, self.V.shape[0])
            arg = [W, H]

        # finally, problem + solver + solution from Pymanopt
        problem = Problem(
            manifold=Product(
                [Euclidean(self.W.shape),
                 Euclidean(self.H.shape)]),
            cost=cost,
            arg=arg,
            verbosity=self.options.verbose)
        solver = ConjugateGradient(
            maxiter=self.options.max_iter,
            maxtime=maxtime,
            mingradnorm=mingradnorm,
            logverbosity=1)
        xopt, extra = solver.solve(
            problem,
            x=[
                np.log(np.maximum(x, self.options.eps))
                for x in (self.W, self.H)  # initial values for W, H
            ])
        # return optimal solution + objective
        return tuple(
            map(lambda x: np.exp(x - logsumexp(x, 1, keepdims=True)),
                xopt)) + (extra['final_values']['f(x)'],
                          extra['final_values']['iterations'])

    def factorize(self, **kwargs):
        """
        Compute and return the matrix factorization as object of class Pmf.
        """
        self.final_obj = sys.float_info.max
        for run in range(self.options.n_run):
            self.W, self.H = self.initialize_nmf()
            self.adjustment()
            if self.options.run_alt_optim is not None:
                self.W, self.H, c_obj, counter = self._run_alt_optim(**kwargs)
            else:
                p_obj = c_obj = sys.float_info.max
                counter = 0
                while self.is_satisfied(p_obj, c_obj, counter):
                    if not self.options.test_conv or counter % self.options.test_conv == 0:
                        p_obj = c_obj
                    self.update()
                    counter += 1
                    if not self.options.test_conv or counter % self.options.test_conv == 0:
                        c_obj = self.objective()
                    if self.options.verbose and (
                            counter <= 5
                            or counter == self.options.test_conv // 2
                            or counter % self.options.test_conv == 0):
                        print("iteration: {}, objective: {}".format(
                            counter, c_obj))
            # if multiple runs are performed, fitted factorization model with
            # the lowest objective function value is retained
            if run == 0 or c_obj <= self.final_obj:
                self.final_obj = c_obj
                self.n_iter = counter
                self.W_best = self.W.copy()
                self.H_best = self.H.copy()
        return self

    def is_satisfied(self, p_obj, c_obj, counter):
        """
        Compute the satisfiability of the stopping criteria based on stopping
        parameters and objective function value.
        """
        if self.options.max_iter and self.options.max_iter <= counter:
            return False
        if self.options.test_conv and counter % self.options.test_conv != 0:
            return True
        if self.options.min_residuals and counter > 0 and \
           p_obj - c_obj < self.options.min_residuals:
            return False
        if counter > 0 and c_obj > p_obj:
            return False
        return True

    def adjustment(self):
        """Adjust small values to avoid numerical underflow."""
        self.H = np.maximum(self.H, self.options.eps)
        self.H /= self.H.sum(1, keepdims=True)
        self.W = np.maximum(self.W, self.options.eps)
        self.W /= self.W.sum(1, keepdims=True)

    def update(self):
        """Update basis and mixture matrix using the EM algorithm."""
        W = self.W.copy()
        H = self.H.copy()
        Vc = self.V.copy()
        Vc.data /= _custom_matmul(W, H, Vc.nonzero())
        self.W = Vc.dot(H.T) * W
        self.W /= self.W.sum(0, keepdims=True)
        self.H = (Vc.T.dot(W)).T * H
        self.H /= self.H.sum(1, keepdims=True)
        self.adjustment()

    def objective(self):
        """Compute weighted Kullback-Leblier divergence cost function."""
        V_est_nz = np.maximum(
            _custom_matmul(self.W, self.H, self.V.nonzero()), self.options.eps)
        # weighted "normalized" KL-divergence
        return (self.obj_offset - np.dot(self.Vrw.data, np.log(V_est_nz))) / \
            self.V.shape[0]

    def __str__(self):
        return self.name

    def basis(self):
        """Return the matrix of basis vectors."""
        return self.H_best

    def coef(self):
        """Return the matrix of mixture coefficients."""
        return self.W_best

    def fitted(self):
        """Compute the estimated target matrix according to the NMF algorithm model."""
        return np.dot(self.W_best, self.H_best)

    def residuals(self):
        """Return residuals matrix between the target matrix and its NMF estimate."""
        return self.V - self.fitted()

    def target(self):
        """Return the target matrix to estimate."""
        return self.V

    def dim(self):
        """
        Return triple containing the dimension of the target matrix and
        matrix factorization rank.
        """
        return self.V.shape + (self.options.rank, )

    def __call__(self, **kwargs):
        """Run the MF algorithm."""
        return self.factorize(**kwargs)
