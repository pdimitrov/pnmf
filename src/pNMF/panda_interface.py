import operator as op
import pickle

import numpy as np
import pandas as pd
import scipy.sparse as sp
from scipy.special import logsumexp

from .pNMF import pNMF


def create_sparse_mat(nnz_i, nnz_j, nnz_val, sp_type=sp.csr_matrix):
    """create sparse matrix from indices/data/type"""
    res = sp_type((nnz_val, (nnz_i, nnz_j)),
                  shape=(nnz_i.max() + 1, nnz_j.max() + 1))
    print('shape: {}, nnzs: {} percent nnz: {}'.format(
        res.shape, res.nnz, res.nnz / np.product(res.shape)))
    return res


def extract_best_clustering(rate_data: pd.DataFrame,
                            original_data: pd.DataFrame,
                            grouping_name: str,
                            aggregating_name: str,
                            id_name: str = None,
                            rate_name: str = "rate",
                            weight_name=None,
                            range_k=range(2, 51),
                            threshold=1e-8,
                            min_num=20,
                            return_all=True,
                            **kwargs):
    """Extract best cluster among the provided range of number of clusters."""
    assert grouping_name in original_data.columns \
        and grouping_name in rate_data.columns
    assert aggregating_name in original_data.columns and \
        aggregating_name in rate_data.columns
    sel_names = [aggregating_name, grouping_name]
    if rate_name in original_data.columns:
        sel_names += [rate_name]
    if id_name is not None:
        assert id_name in original_data.columns
        sel_names += [id_name]

    def normalize_rates(df):
        if (df[rate_name] < threshold).all():
            return df[:0]
        log_rates = np.log(df[rate_name].values)
        return df.assign(rate=np.exp(log_rates - logsumexp(log_rates)))

    def fix(df, combine=False):
        df[aggregating_name] = df[aggregating_name].astype("category")
        df[grouping_name] = df[grouping_name].astype("category")
        if combine:
            df["grp_id"] = df[grouping_name]
            if id_name is not None:
                df["grp_id"] = df["grp_id"].astype(str).str.cat(
                    df[id_name].astype(str), sep="::").astype("category")
                df[id_name] = df[id_name].astype("category")
        return df

    # prepare rates data frame
    print("Filtering/normalizing...")
    rates = rate_data[rate_data[grouping_name].isin(original_data[grouping_name])]\
        .groupby(aggregating_name)\
        .apply(lambda df: df[:0] if df.shape[0] < min_num else df)\
        .reset_index(drop=True)\
        .groupby(grouping_name)\
        .apply(normalize_rates)\
        .reset_index(drop=True)

    print("Intersecting with the original data/more filtering...")
    idata = original_data
    idata = idata.loc[
        idata[aggregating_name].isin(rates[aggregating_name]) &
        idata[grouping_name].isin(rates[grouping_name]),
        sel_names]\
        .copy()\
        .pipe(fix, combine=True)\
        .reset_index(drop=True)
    rates = rates.loc[rates[aggregating_name].isin(idata[aggregating_name])]\
        .pipe(fix)
    if rates[grouping_name].nunique() < rate_data[grouping_name].nunique():
        print("number of unique grouping IDs is {} down from {}\n".format(
            rates[grouping_name].nunique(),
            rate_data[grouping_name].nunique()))
    if weight_name is not None:
        row_weights = pd.DataFrame({"grp": rates[grouping_name].cat.codes.values,
                                    "weight": rates[weight_name]})\
            .drop_duplicates()\
            .sort_values("grp")["weight"].values
    else:
        row_weights = None
    print("X-", end='')
    X = create_sparse_mat(rates[grouping_name].cat.codes.values,
                          rates[aggregating_name].cat.codes.values,
                          rates.rate.values)
    print("Y-", end='')
    Y = create_sparse_mat(idata.grp_id.cat.codes.values,
                          idata[aggregating_name].cat.codes.values,
                          idata[rate_name] if rate_name in idata.columns
                          else np.ones(idata.shape[0]),
                          sp.coo_matrix)\
        .tocsr()
    match_index = pd.Series(idata.grp_id.cat.categories.astype(str))\
        .str.split("::")\
        .map(op.itemgetter(0))\
        .astype("category")\
        .cat.codes.values
    assert rates[grouping_name]\
        .astype("category")\
        .cat.codes.isin(match_index)\
        .mean() == 1
    BICs, grouping, fit, scores = pNMF(X, Y, match_index, row_weights, **kwargs)\
        .range_clustering(range_k, return_all, **kwargs)

    def to_df(grouping=grouping, scores=scores):
        return pd.DataFrame({
            'id': rates[grouping_name].cat.categories,
            "grouping": grouping,
            "scores": scores
        })

    mapping = pd.concat([
        to_df(gr, sc).assign(k=k)
        for k, gr, sc in zip(BICs.index, grouping, scores)
    ]) if return_all else to_df()
    return BICs, mapping, fit, aggregating_name
