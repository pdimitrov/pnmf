#!/usr/bin/env python3

import argparse
import csv
import re
import sys

import pandas as pd

# global constants and options
# pd.options.display.max_columns = 100
parser = argparse.ArgumentParser(
    "process_raw_data", description='Process raw data.')
parser.add_argument(
    'input_file_name',  # input.csv
    metavar="'input file name'",
    type=str,
    help="""name of the input csv/tsv file""")
parser.add_argument(
    'grouping_factor',  #
    metavar="'grouping factor'",
    type=str,
    help="""grouping factor name""")
parser.add_argument(
    'aggregating_factor',  #
    metavar="'aggregating factor'",
    type=str,
    help="""aggregating factor""")
parser.add_argument(
    '--id_factor',  #
    metavar="'id factor name'",
    type=str,
    default=None,
    help="""id factor name""")
parser.add_argument(
    '--distinct',  #
    metavar="'distinct counting'",
    type=bool,
    default=True,
    help="""distinct counting""")
parser.add_argument(
    '--min_number_ids',  #
    metavar="'min_number_ids'",
    type=int,
    default=50,
    help="""min number ids""")
parser.add_argument(
    '--output_file_name',
    metavar="'output file name'",
    type=str,
    default=None,  # output.csv
    help="""name of the output csv/tsv file""")


def extract_summary(
        idata: pd.DataFrame,  #
        grouping_factor: str,
        aggregating_factor: str,
        id_factor: str = None,
        min_number_ids: int = 50,
        distinct=False):
    print("remove missing/crap data...")
    sel_factors = [grouping_factor, aggregating_factor]
    if id_factor is not None:
        sel_factors += [id_factor]
    idata = idata[sel_factors]
    if min_number_ids is not None:
        print(
            f"filter grouping factor by min_number_ids (>= {min_number_ids})..."
        )
        tbl = idata\
            .drop_duplicates([grouping_factor, id_factor])[grouping_factor]\
            .value_counts()
        idata = idata[idata[grouping_factor].isin(
            tbl.index[tbl >= min_number_ids])]
    print(f"summarize {idata[grouping_factor].nunique()} groups...")

    def f(df):
        gr = df[grouping_factor].iat[0]
        print(f"group id: {gr}")
        tbl = pd.crosstab(df[id_factor], df[aggregating_factor])
        if distinct:
            tbl = (tbl > 0) + 0
        return tbl\
            .reset_index()\
            .melt([id_factor], value_name="count")\
            .groupby(aggregating_factor)\
            .apply(lambda df:
                   df["count"].value_counts(sort=False).reset_index())\
            .reset_index(level="section")\
            .rename(columns={"index": "x"})

    return idata\
        .groupby(grouping_factor)\
        .apply(f)\
        .reset_index(level=grouping_factor)


def process_inputs(input_file_name,
                   grouping_factor,
                   aggregating_factor,
                   id_factor=None,
                   output_file_name=None,
                   min_number_ids=50,
                   distinct=True):
    input_sep = "\t" if re.search(r"\.csv$", input_file_name) is None else ","
    output_sep = "\t" if re.search(r"\.csv$",
                                   output_file_name) is None else ","

    print(f"processing {input_file_name} file...")
    records = []
    columns = [grouping_factor, aggregating_factor, id_factor]
    with open(input_file_name, 'r') as csvfile:
        for ii, row in enumerate(csv.DictReader(csvfile, delimiter=input_sep)):
            records += [
                dict(item for item in row.items() if item[0] in columns)
            ]
            if ii % 1000000 == 0:
                print(f"line {ii + 1}")
    idata = pd.DataFrame.from_records(records)[
        lambda df: df[aggregating_factor] != ""]
    del records
    res = extract_summary(idata,
                          grouping_factor,
                          aggregating_factor,
                          id_factor,
                          min_number_ids,
                          distinct)\
        .reset_index(drop=True)
    if output_file_name is not None:
        print("saving the raw results...")
        res.to_csv(
            re.sub(r"(\.[t|c]sv)$", r"_raw\1", output_file_name),
            index=False,
            sep=output_sep)
    print("second stage of summarization...")
    col_ind = [grouping_factor, aggregating_factor]
    res = res\
        .groupby(col_ind)\
        .apply(lambda df:
               pd.DataFrame({"w": [df["count"].sum()],
                             "count": [sum(df.loc[df.x > 0, "count"])]}))\
        .reset_index(level=col_ind)
    print(f"saving resulting dataframe to {output_file_name}...")
    res.to_csv(
        sys.stdout if output_file_name is None else output_file_name,
        index=False,
        sep=output_sep)


if __name__ == '__main__':

    input_args = parser.parse_args()
    if False:
        input_args = parser.parse_args(command.split(" "))

    process_inputs(
        input_args.input_file_name,
        input_args.grouping_factor,
        input_args.aggregating_factor,
        id_factor=input_args.id_factor,
        output_file_name=input_args.output_file_name,
        min_number_ids=input_args.min_number_ids,
        distinct=input_args.distinct)
