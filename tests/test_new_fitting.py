import numpy as np
import pandas as pd
import pytest
from scipy.special import logsumexp

import autograd.numpy as anp
from autograd import grad
from autograd.scipy.misc import logsumexp as ag_logsumexp
from pNMF import Pmf
from pymanopt import Problem
from pymanopt.manifolds import Euclidean, Product
from pymanopt.solvers import ConjugateGradient

from .common import create_data

pytestmark = pytest.mark.filterwarnings("ignore")


def _custom_matmul(X, Y, nnz_indices):
    """Fast matrix multiplication of two dense matrices given a set of indexes
    where they need to be multiplied."""
    assert X.shape[1] == Y.shape[0]
    assert nnz_indices[0].max() < X.shape[0]
    assert nnz_indices[1].max() < Y.shape[1]
    return np.sum(X[nnz_indices[0], :] * Y[:, nnz_indices[1]].T, 1)


def test_new(seed=11116, maxtime=1800, mingradnorm=1e-7):
    X1, X2, X, X_norm, true_labels = create_data(seed)
    pnmf0 = Pmf(
        X_norm,
        rank=2,
        run_alt_optim="TF",
        model_type="KL",
        row_weights=np.asarray(np.concatenate([X1, X2]).sum(1, keepdims=True)),
        row_normalized=True,
        n_run=1,
        verbose=1,
        max_iter=2000,
        random_seed=seed)
    if False:
        pnmf0()
        out = (pd.Series(pnmf0.coef().argmax(1)) != true_labels).mean()
    # create the two empty Tensorflow variables W and H to be fed by
    # the Pymanopt's Problem/Solver instances below
    pnmf = Pmf(
        X_norm,
        rank=2,
        run_alt_optim="TF",
        model_type="KL",
        row_weights=np.asarray(np.concatenate([X1, X2]).sum(1, keepdims=True)),
        row_normalized=True,
        n_run=1,
        verbose=1,
        max_iter=2000,
        random_seed=seed)
    pnmf.W, pnmf.H = pnmf.initialize_nmf()
    pnmf.adjustment()
    rat = pnmf.Vrw.copy()  # use the shape
    Vrw = np.asarray(pnmf.Vrw.todense())
    use_numpy = True

    def convert(x):
        # if use_numpy:
        #     return np.exp(x - logsumexp(x, 1, keepdims=True))
        # return anp.exp(x - ag_logsumexp(x, 1, keepdims=True))
        if use_numpy:
            return np.asarray(np.exp(x))
        return anp.exp(x)

    def cost(x):
        if use_numpy:
            pnmf.W, pnmf.H = convert(x[0]), convert(x[1])
            print((pnmf.W[0, :], pnmf.H[:, 0]))
            return pnmf.objective()
        lV_est = anp.log(anp.dot(convert(x[0]), convert(x[1])))
        return (pnmf.obj_offset - anp.sum(Vrw * lV_est)) / Vrw.shape[0]

    def my_grad(x):
        W, H = np.asarray(convert(x[0])), np.asarray(convert(x[1]))
        rat.data = pnmf.Vrw.data / _custom_matmul(W, H, pnmf.Vrw.nonzero())
        return [
            np.multiply(rat.dot(pnmf.H.T), W) / -Vrw.shape[0],
            np.multiply(W.T.dot(rat.todense()), H) / -Vrw.shape[0]
        ]

    # finally, problem + solver + solution from Pymanopt
    problem = Problem(
        manifold=Product([Euclidean(pnmf.W.shape),
                          Euclidean(pnmf.H.shape)]),
        cost=cost,
        egrad=my_grad,
        verbosity=pnmf.options.verbose)
    solver = ConjugateGradient(
        maxiter=pnmf.options.max_iter,
        maxtime=maxtime,
        mingradnorm=mingradnorm,
        logverbosity=1)

    x0 = [np.log(np.asarray(x)) for x in (pnmf.W, pnmf.H)]
    # mgg = my_grad(x0)
    # ggg = grad(cost)(x0)  # cost(x0)
    xopt, extra = solver.solve(problem, x=x0)
    import ipdb
    ipdb.set_trace()
    tuple(map(lambda x: np.exp(x - logsumexp(x, 1, keepdims=True)), xopt)) + (
        extra['final_values']['f(x)'], extra['final_values']['iterations'])
    assert pytest.approx(min(out, 1. - out)) == .0225


def test_new1(seed=11116, maxtime=1800, mingradnorm=1e-7):
    X1, X2, X, X_norm, true_labels = create_data(seed)
    pnmf0 = Pmf(
        X_norm,
        rank=2,
        run_alt_optim="TF",
        model_type="KL",
        row_weights=np.asarray(np.concatenate([X1, X2]).sum(1, keepdims=True)),
        row_normalized=True,
        n_run=1,
        verbose=1,
        max_iter=2000,
        random_seed=seed)
    if False:
        pnmf0()
        out = (pd.Series(pnmf0.coef().argmax(1)) != true_labels).mean()
    # create the two empty Tensorflow variables W and H to be fed by
    # the Pymanopt's Problem/Solver instances below
    pnmf = Pmf(
        X_norm,
        rank=2,
        run_alt_optim="TF",
        model_type="KL",
        row_weights=np.asarray(np.concatenate([X1, X2]).sum(1, keepdims=True)),
        row_normalized=True,
        n_run=1,
        verbose=1,
        max_iter=2000,
        random_seed=seed)
    pnmf.W, pnmf.H = pnmf.initialize_nmf()
    pnmf.adjustment()
    rat = pnmf.Vrw.copy()  # use the shape
    Vrw = np.asarray(pnmf.Vrw.todense())
    use_numpy = True

    def convert(x):
        # if use_numpy:
        #     return np.exp(x - logsumexp(x, 1, keepdims=True))
        # return anp.exp(x - ag_logsumexp(x, 1, keepdims=True))
        if use_numpy:
            return np.asarray(np.exp(x))
        return anp.exp(x)

    def cost(x):
        if use_numpy:
            pnmf.W, pnmf.H = convert(x[0]), convert(x[1])
            print((pnmf.W[0, :], pnmf.H[:, 0]))
            return pnmf.objective()
        lV_est = anp.log(anp.dot(convert(x[0]), convert(x[1])))
        return (pnmf.obj_offset - anp.sum(Vrw * lV_est)) / Vrw.shape[0]

    def my_grad(x):
        W, H = np.asarray(convert(x[0])), np.asarray(convert(x[1]))
        rat.data = pnmf.Vrw.data / _custom_matmul(W, H, pnmf.Vrw.nonzero())
        return [
            np.multiply(rat.dot(pnmf.H.T), W) / -Vrw.shape[0],
            np.multiply(W.T.dot(rat.todense()), H) / -Vrw.shape[0]
        ]

    # finally, problem + solver + solution from Pymanopt
    problem = Problem(
        manifold=Product([Euclidean(pnmf.W.shape),
                          Euclidean(pnmf.H.shape)]),
        cost=cost,
        egrad=my_grad,
        verbosity=pnmf.options.verbose)
    solver = ConjugateGradient(
        maxiter=pnmf.options.max_iter,
        maxtime=maxtime,
        mingradnorm=mingradnorm,
        logverbosity=1)

    x0 = [np.log(np.asarray(x)) for x in (pnmf.W, pnmf.H)]
    # mgg = my_grad(x0)
    # ggg = grad(cost)(x0)  # cost(x0)
    xopt, extra = solver.solve(problem, x=x0)
    import ipdb
    ipdb.set_trace()
    tuple(map(lambda x: np.exp(x - logsumexp(x, 1, keepdims=True)), xopt)) + (
        extra['final_values']['f(x)'], extra['final_values']['iterations'])
    assert pytest.approx(min(out, 1. - out)) == .0225
