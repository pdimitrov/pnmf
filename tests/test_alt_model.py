import numpy as np
import pandas as pd
import pytest
from pNMF import Pmf

from .common import create_data

pytestmark = pytest.mark.filterwarnings("ignore")


def test_AD(seed=11116):
    """testing Pmf_clustering function with Autograd"""
    X1, X2, X, X_norm, true_labels = create_data(seed)
    print("running test: 1")
    pnmf = Pmf(
        X_norm,
        rank=2,
        run_alt_optim="AD",
        model_type="JS",
        row_weights=np.asarray(np.concatenate([X1, X2]).sum(1, keepdims=True)),
        row_normalized=True,
        n_run=3,
        verbose=1,
        random_seed=seed)
    pnmf()
    out = (pd.Series(pnmf.coef().argmax(1)) != true_labels).mean()
    assert pytest.approx(min(out, 1. - out)) == .0225


def test_TF_JS(seed=11116):
    """testing Pmf_clustering function with Tensorflow for
    Jensen-Shannon discrepancy"""
    X1, X2, X, X_norm, true_labels = create_data(seed)
    print("running test with rank = 2")
    pnmf = Pmf(
        X_norm,
        rank=2,
        run_alt_optim="TF",
        model_type="JS",
        row_weights=np.asarray(np.concatenate([X1, X2]).sum(1, keepdims=True)),
        row_normalized=True,
        n_run=3,
        verbose=1,
        max_iter=2000,
        random_seed=seed)
    pnmf()
    out = (pd.Series(pnmf.coef().argmax(1)) != true_labels).mean()
    assert pytest.approx(min(out, 1. - out)) == .0225

    print("running test with rank = 3")
    pnmf = Pmf(
        X_norm,
        rank=3,
        run_alt_optim="TF",
        model_type="JS",
        row_weights=np.asarray(np.concatenate([X1, X2]).sum(1, keepdims=True)),
        row_normalized=True,
        n_run=3,
        verbose=1,
        max_iter=2000,
        random_seed=seed)
    pnmf()
    out = pd.Series(pnmf.coef().argmax(1))
    assert (out.value_counts().values == np.array([994, 529, 477])).all()


def test_TF_KL(seed=11116):
    """testing Pmf_clustering function with Tensorflow for
    Kullback-Leblier discrepancy"""
    X1, X2, X, X_norm, true_labels = create_data(seed)
    pnmf = Pmf(
        X_norm,
        rank=2,
        run_alt_optim="TF",
        model_type="KL",
        row_weights=np.asarray(np.concatenate([X1, X2]).sum(1, keepdims=True)),
        row_normalized=True,
        n_run=3,
        verbose=1,
        max_iter=2000,
        random_seed=seed)
    pnmf()
    out = (pd.Series(pnmf.coef().argmax(1)) != true_labels).mean()
    assert pytest.approx(min(out, 1. - out)) == 0.0225
