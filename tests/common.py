import numpy as np
import pandas as pd
import scipy.sparse as sp


def create_data(seed=11116, N=1000):
    def create_p(rawp, alpha=7.5e-2):
        rawp += alpha
        return rawp / rawp.sum()

    np.random.seed(seed)
    X1 = np.random.multinomial(
        20, create_p(np.concatenate([np.ones(40), np.zeros(40)])), N)
    X2 = np.random.multinomial(
        20, create_p(np.concatenate([np.zeros(40), np.ones(40)])), N)
    X = sp.csr_matrix(np.concatenate([X1, X2])).astype("float64")
    X_norm = sp.csr_matrix(
        np.concatenate(
            [X1 / X1.sum(1, keepdims=True),
             X2 / X2.sum(1, keepdims=True)])).astype("float64")
    print('\nshape: ', X.shape, ' nnzs: ', X.nnz, ' percent nnz: ',
          X.nnz / np.product(X.shape))
    true_labels = pd.Series(([1] * N) + ([0] * N))
    return X1, X2, X, X_norm, true_labels
