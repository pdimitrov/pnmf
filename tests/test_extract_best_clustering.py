import pickle

import numpy as np
import pandas as pd
import pytest
from pkg_resources import resource_filename

from pNMF import extract_best_clustering

pytestmark = pytest.mark.filterwarnings("ignore")


def get_filename(name: str):
    return resource_filename("pNMF", "data/" + name)


def test_extract_best_clustering(verbose=True, seed=11116):
    """testing Pmf_clustering function"""

    np.random.seed(seed)
    ex_data = pd.read_csv(get_filename("example_data.csv"))
    summ_data = pd.read_csv(get_filename("summary_data.csv"))
    # performing_grouping vs. aggregating factor clustering,
    # using the id_factor for counting
    print("\nrunning test: 1")
    best_fit = extract_best_clustering(
        summ_data,
        ex_data,
        grouping_name="grouping_factor",
        aggregating_name="aggregating_factor",
        id_name="id_factor",
        rate_name="count",
        weight_name="weight",
        range_k=range(10, 21),
        random_seed=seed,
        verbose=verbose,
        n_run=3)
    # with open("data/test_extract_best_clustering1.pic", "wb") as f:
    #     pickle.dump(best_fit, f)
    with open(get_filename("test_extract_best_clustering1.pic"), "rb") as f:
        BICs, mapping, fit, aggregating_name = pickle.load(f)
        assert (BICs == best_fit[0]).mean() == 1.
        assert (mapping == best_fit[1]).mean().mean() == 1.
        assert aggregating_name == "aggregating_factor"

    # performing_grouping vs. aggregating factor clustering,
    # using the id_factor for counting
    print("\nrunning test: 2")
    best_fit = extract_best_clustering(
        summ_data,
        summ_data,
        grouping_name="grouping_factor",
        aggregating_name="aggregating_factor",
        rate_name="count",
        weight_name="weight",
        range_k=range(10, 21),
        random_seed=seed,
        verbose=verbose,
        n_run=3)
    # with open("data/test_extract_best_clustering2.pic", "wb") as f:
    #     pickle.dump(best_fit, f)
    with open(get_filename("test_extract_best_clustering2.pic"), "rb") as f:
        BICs, mapping, fit, aggregating_name = pickle.load(f)
        assert (BICs == best_fit[0]).mean() == 1.
        assert (mapping == best_fit[1]).mean().mean() == 1.
        assert aggregating_name == "aggregating_factor"
