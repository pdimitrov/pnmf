import numpy as np
import pandas as pd
import pytest

from pNMF import Pmf

from .common import create_data

pytestmark = pytest.mark.filterwarnings("ignore")


def test_Pmf(verbose=False, seed=11116):
    """testing Pmf_clustering function"""
    X1, X2, X, X_norm, true_labels = create_data(seed)
    print("running test: 1")
    # no row normalization/weights
    pnmf = Pmf(
        X,
        rank=2,
        n_run=3,
        row_normalized=False,
        verbose=verbose,
        random_seed=seed)
    pnmf()
    assert (pd.Series(pnmf.coef().argmax(1)) == 1).mean() == .5

    print("running test: 2")
    pnmf = Pmf(
        X,
        rank=3,
        n_run=3,
        row_normalized=False,
        verbose=verbose,
        random_seed=seed)
    pnmf()
    ser = pd.Series(pnmf.coef().argmax(1))
    tbl = ser.value_counts(sort=False)
    assert (tbl == [504, 496, 1000]).all()

    # with row normalization/weights
    print("running test: 3")
    pnmf = Pmf(
        X_norm,
        rank=2,
        row_weights=np.asarray(X.sum(1)).reshape((X.shape[0], 1)),
        row_normalized=True,
        n_run=3,
        verbose=verbose,
        random_seed=seed)
    pnmf()
    assert (pd.Series(pnmf.coef().argmax(1)) == 1).mean() == .5
